# -*- mode: python -*-

block_cipher = None


a = Analysis(['source/SQL/__pycache__/__init__.cpython-36.pyc', 'source/SQL/__pycache__/admin_statements.cpython-36.pyc', 'source/SQL/__pycache__/build_tables.cpython-36.pyc', 'source/SQL/__pycache__/copier_statements.cpython-36.pyc', 'source/SQL/__pycache__/copier_statments.cpython-36.pyc', 'source/SQL/__pycache__/copiers.cpython-36.pyc', 'source/SQL/__pycache__/db_interface.cpython-36.pyc', 'source/code/__pycache__/__init__.cpython-36.pyc', 'source/code/__pycache__/copier_dialogs.cpython-36.pyc', 'source/code/__pycache__/copiers.cpython-36.pyc'],
             pathex=['/home/brian/Projects/copierOrganizer'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='__init__.cpython-36',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='__init__.cpython-36')
