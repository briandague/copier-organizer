# -*- mode: python -*-

block_cipher = None


a = Analysis(['source/copier_organizer.py'],
             pathex=['/home/brian/Projects/copierOrganizer'],
             binaries=[],
             datas=[('source/views', 'views'), ('source/SQL', 'SQL'), ('source/error_handling', 'error_handling'), ('source/dialogs', 'dialogs')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='copier_organizer',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='copier_organizer')
