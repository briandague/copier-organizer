import sys, os
from PyQt5 import QtWidgets, QtCore, QtGui, uic
from SQL import build_tables, copier_statements, admin_statements, location_statements, account_statements, locations_accounts_join
from error_handling.error_handler import handle_error
from dialogs.copier_dialog import CopierDataForm
from dialogs.admin_dialogs import AdminForm
from dialogs.account_dialogs import AccountForm
from dialogs.location_dialogs import LocationForm
from dialogs.CSV_dialog import CSVForm
from csv_writer.csv_builder import write_csv
'''
    Starting the application opens up the app onto the MAIN PAGE
        -> Shows a view of every copier in the database
        -> Has a search bar that alters the view to only copiers that fit the search criteria

    Dependencies: main_page.py, main_page.ui
'''
PATH = os.path.dirname(os.path.realpath(__file__))

DB_URI = os.path.join(PATH, 'SQL/copier_database.db')

database = build_tables.get_db_connection(DB_URI)

qtCreatorFile = os.path.join(PATH, 'views/main_page.ui')

Ui_MainWindow, QtBaseClass=uic.loadUiType(qtCreatorFile)


class MainPage(QtWidgets.QMainWindow, Ui_MainWindow):

    """Code for page that opens when the app starts.
        The page has a search bar and a table containing all the copiers in the database.
        This File should contian code that facilitates the interaction between 
        the Interface, database, and logic. 
        All Functions here should be written in main_page.py and passed into here.
        Methods in this class should work like this:
        1. Get data from a form from the Dialogs class.
        2. Use a method from the statement's SQL class to add it into the database
    """

    def __init__(self):
        """ Initializer for the Main Page
            Opens up the app and shows all the info about the copiers
            All the menus, buttons, and other user interactions 
            should be bound to a class method.
        """
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        #Each of these classes contains SQL code for interacting with the database
        #See source/SQL/DatabaseInterface for a better understanding
        self.copier_actions = copier_statements.CopierStatements(database)
        self.admin_actions = admin_statements.AdminStatements(database)
        self.account_actions = account_statements.AccountStatements(database)
        self.location_actions = location_statements.LocationStatements(database)
        self.join_actions = locations_accounts_join.JoinAccountsLocations(database)

        self.fill_table(self.copier_actions.get_all())
        self.selected = None

        self.copierTable.cellClicked.connect(self.set_selected)
        #Button that runs delete copier
        self.remove_copier_button.clicked.connect(self.remove_copier)
        #Button that opens the add copier dialog
        self.add_copier_button.clicked.connect(self.add_new_copier)
        #Button that opens the alter copier dialog
        self.modify_copier_button.clicked.connect(self.modify_existing_copier)
        #Button to run the search
        self.search_button.clicked.connect(self.search)

        #Menu items - the "Add" menu
        self.actionAdmin.triggered.connect(self.add_new_admin)
        self.actionAccount.triggered.connect(self.add_new_account)
        self.actionLocation.triggered.connect(self.add_new_location)
        self.actionNew_CSV.triggered.connect(self.open_CSV_form)
        

    def set_selected(self, row, column):
         """
         Selected is the variable used in update and delete queries, setby the user by clicking 
         the table
         """
         self.selected = self.copierTable.item(row, 0).text()
         self.selectedLabel.setText(self.selected)

    def search(self):
        """
        Enter a full Copier Serial Number. Returns all Copier containg that pattern
        in their Serial Number
        TODO: Add the ability to searchby any field
        :returns: the Result of a DB query that matches that pattern
        """
        pattern = self.searchText.toPlainText()
        if pattern:
            found = [self.copier_actions.search(str(pattern))]
            self.fill_table(found)
        else:
            self.fill_table(self.copier_actions.get_all())


    @handle_error
    def add_new_copier(self):
        """
        opens a new copier dialog, gets a SQL statment based off of it, and commits it
        """ 
        #for filling in the combo boxes in the copier dialog
        admins = self.admin_actions.get_all()
        locations = self.location_actions.get_all()
        admin_names = [admin[1] for admin in admins]
        location_names = [location[1] for location in locations]
        
        new_copier = CopierDataForm(admins=sorted(admin_names), locations=sorted(location_names))
        inserted = new_copier.add_copier()
        self.copier_actions.insert_copier(inserted)
        self.fill_table(self.copier_actions.get_all())

    def remove_copier(self, copier):
        """
        Deletes a copier from the database
        :copier: Whatever copier self.selected is at the datetime
        """
        self.copier_actions.remove(self.selected)
        self.fill_table(self.copier_actions.get_all())

    def modify_existing_copier(self):
        """
        Opens an edit copier dialog
        """
        if not self.selected:
            return
        admins = self.admin_actions.get_all()
        admin_names = [admin[1] for admin in admins]
        locations = self.location_actions.get_all()
        location_names = [location[1] for location in locations]
        modified_copier = CopierDataForm(admins=admin_names, locations=location_names)
        change_copier = self.copier_actions.search(self.selected)
        if not modified_copier:
            return
        modified = modified_copier.modify_copier(change_copier)
        self.copier_actions.modify_copier(modified)
        self.fill_table(self.copier_actions.get_all())

    def fill_table(self, data):

        """
        Puts data in the main table on the front of the page
        :data: List of tuples or lists (A sqlite3 query, preferably)
        """
        if not data:
            self.copierTable.clear()
            return

        self.copierTable.setRowCount(len(data))
        self.copierTable.setColumnCount(len(data[0]))
        row = 0
        for item in data:
            column = 0
            for field in item:
                #Prices will not show up in the table unless str(fields) is used
                self.copierTable.setItem(row, column, QtWidgets.QTableWidgetItem(str(field)))
                column += 1
            row+=1
        self.copierTable.setHorizontalHeaderLabels(
                ['S/N', 'IP', 'Hostname', 'Room #', 'Model #', 'Paper Source', 'Finisher', 
                    'Stapler Number', 'BW Click','Color Click', 'Location_ID', 'Admin']
                )

    @handle_error
    def add_new_admin(self):
        """
        Opens a dialog for adding admins
        """
        new_admin = AdminForm()
        data = new_admin.add_admin()
        self.admin_actions.insert_admin(data)

    @handle_error
    def add_new_account(self):
        """
        Opens a dialog for adding admins
        """
        locations = self.location_actions.get_all()
        location_names = [str(location[0]) for location in locations]
        new_account = AccountForm(location_names)
        data = new_account.add_account()
        self.account_actions.insert_account(data)

    @handle_error
    def add_new_location(self):
        """
        Opens a dialog for adding locations
        """
        new_location = LocationForm()
        data = new_location.add_location()
        self.location_actions.insert_location(data)

    def open_CSV_form(self):
        '''
        opens a special form for editing new CSV files
        '''
        csv_data = self.join_actions.join_accounts_locations()
        csv_form = CSVForm(csv_data)
        items = csv_form.get_data()
        csv_file = QtWidgets.QFileDialog.getSaveFileName(self,
                'Save CSV',
                "",
                '.csv'
        )
        csv_file = csv_file[0]+csv_file[1]
        write_csv(csv_file, items)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = MainPage()
    window.show()
    sys.exit(app.exec_())

main()
