import os, sqlite3, logging, sys

logger = logging.getLogger()
log_path = (os.path.join(os.path.dirname(__file__), '../logs/logging.txt'))

if not os.path.exists(log_path):
    os.mkdir(os.path.join(os.path.dirname(__file__), '../logs'))
    os.mknod(log_path)

log_handler = logging.FileHandler(log_path)
log_format = logging.Formatter('%(asctime)s:%(message)s')
logger.addHandler(log_handler)

tables = '''

CREATE TABLE IF NOT EXISTS admins (
    id INTEGER PRIMARY KEY,
    name text,
    email text,
    phone text
);

CREATE TABLE IF NOT EXISTS locations (
    location_id int PRIMARY_KEY,
    department text
);

CREATE TABLE IF NOT EXISTS  copiers (
    copier_serial text PRIMARY KEY,
    ipaddress text,
    hostname text,
    room text,
    model text,
    paper_source text,
    finisher text,
    stapler_number text,
    price real,
    color_price real,
    location_id int,
    admin text,
    FOREIGN KEY(location_id) REFERENCES locations(location_id),
    FOREIGN KEY(admin) REFERENCES copieradmins(admin)
);

CREATE TABLE IF NOT EXISTS copieradmins (
    id int PRIMARY KEY,
    admin text,
    copier text,
    FOREIGN KEY(copier) REFERENCES copiers(copier_serial),
    FOREIGN KEY(admin) REFERENCES admins(name)
);

CREATE TABLE IF NOT EXISTS accounts (
    speedtype int PRIMARY KEY,
    department text,
    location_id int,
    FOREIGN KEY(location_id) REFERENCES locations(location_id)
);

CREATE TABLE IF NOT EXISTS toner (
    toner_number text PRIMARY KEY,
    copier text,
    FOREIGN KEY(copier) REFERENCES copiers(copier_serial)
);

CREATE TABLE IF NOT EXISTS waste_toner (
    waste_toner_number text PRIMARY KEY,
    toner text,
    FOREIGN KEY(toner) REFERENCES toner(toner_number)
);

'''

def get_db_connection(path):
    connection = sqlite3.connect(path)
    cursor = connection.cursor()
    try:
        cursor.executescript(tables)
        return connection
    except (sqlite3.OperationalError):
        connection.rollback()
        logger.critical("Could not find or build database sucessfully: {}".format(sys.exc_info()))
