from .db_interface import DatabaseInterface

class LocationStatements(DatabaseInterface):

    """API for interacting with the location table"""

    def __init__(self, connection):
        """Constructior for the location interfece, input an established connection.

        :connection: Pre-established sqlite3 connection

        """
        DatabaseInterface.__init__(self, connection)

        self.insert_statement = 'INSERT INTO locations(location_id, department)  VALUES (?, ?)'
        self.update_statement = 'UPDATE locations SET department=? WHERE location_id=?'
        self.remove_statement = 'DELETE FROM locations WHERE location_id=?'
        self.search_statement = 'SELECT * FROM locations WHERE location_id=?'
        self.select_all = 'SELECT * FROM locations'

    def insert_location(self, data):
        """method for adding locationto the database

        :data: Admin's location_id, email, and phone number, in a tuple, as strings

        """
        self.run_input_statement(self.insert_statement, data)

    def modify_location(self, data):
        """For changing an location's phone or email

        :data: Admin's location_id, email, and phone number, in a tuple, as strings

        """
        structured_data = (data[1], data[0])
        self.run_input_statement(self.update_statement, structured_data)

    def remove_location(self, location_location_id):
        """Deletes an location from the database

        :location_location_id: location_id, as string, of the location to be deleted 
        :returns: TODO

        """
        self.run_input_statement(self.remove_statement, (location_location_id,))

    def get_all(self):
        """Returns all the locations in the database
        :returns: every instance oflocationin the database, a hail mary 

        """
        return self.run_output_statement(self.select_all)
