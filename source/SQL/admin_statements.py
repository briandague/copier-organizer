from .db_interface import DatabaseInterface

class AdminStatements(DatabaseInterface):

    """API for interacting with the admin table"""

    def __init__(self, connection):
        """Constructior for the admin interfece, input an established connection.

        :connection: Pre-established sqlite3 connection

        """
        DatabaseInterface.__init__(self, connection)

        self.insert_statement = 'INSERT INTO admins(name, email, phone)  VALUES (?, ?, ?)'
        self.update_statement = 'UPDATE admins SET email=?,phone=? WHERE name=?'
        self.remove_statement = 'DELETE FROM admins WHERE name=?'
        self.search_statement = 'SELECT * FROM admins WHERE name=?'
        self.select_all = 'SELECT * FROM admins'

    def insert_admin(self, data):
        """method for adding adminto the database

        :data: Admin's name, email, and phone number, in a tuple, as strings

        """
        self.run_input_statement(self.insert_statement, data)

    def modify_admin(self, data):
        """For changing an admin's phone or email

        :data: Admin's name, email, and phone number, in a tuple, as strings

        """
        structured_data = (data[1], data[2], data[0])
        self.run_input_statement(self.update_statement, structured_data)

    def remove_admin(self, admin_name):
        """Deletes an admin from the database

        :admin_name: name, as string, of the admin to be deleted, not the primary key 
        :returns: TODO

        """
        self.run_input_statement(self.remove_statement, (admin_name,))

    def get_all(self):
        """Returns all the admins in the database
        :returns: TODO

        """
        return self.run_output_statement(self.select_all)

    def find_admin(self, data):
        return self.run_output_statement(self.search_statement, (data,))
