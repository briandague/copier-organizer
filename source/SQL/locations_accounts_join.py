from .db_interface import DatabaseInterface

class JoinAccountsLocations(DatabaseInterface):

    """API for interacting with the admin table"""

    def __init__(self, connection):
        """Constructior for the admin interfece, input an established connection.

        :connection: Pre-established sqlite3 connection

        """
        DatabaseInterface.__init__(self, connection)

        self.join_statement = 'SELECT * FROM locations INNER JOIN accounts on locations.location_id=accounts.location_id'

    def join_accounts_locations(self):
        """Returns all the admins in the database
        :returns: TODO

        """
        return self.run_output_statement(self.join_statement)

