import csv
from SQL import build_tables
import sqlite3
from SQL import account_statements 


connection = build_tables.get_db_connection('SQL/copier_database.db')
account_actions = account_statements.AccountStatements(connection)


with open('Accounts.csv', 'r') as accounts:
    #for each line in that file
    reader = csv.reader(accounts)
    for line in reader:
        #separate the thierd line on the _, keep the second part
        third = line[2].split('_')[1]
        #add account, speedtype
        print("{} {} {}".format(third, line[1], line[0]))
        try:
            account_actions.insert_account((third, line[1], line[0]))
        except sqlite3.IntegrityError:
            print(third)
            pass
