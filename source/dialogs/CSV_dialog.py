from .abstract_data_form import DataForm
import os
from PyQt5 import QtWidgets


UI_admin = '../views/CSV_dialog.ui'

UI_PATH = os.path.join(os.path.dirname(__file__), UI_admin)
class CSVForm(DataForm):

    """Form for writing the CSVs
       CSVs are comma-separated values used for wiriting billing 
       statatements in a way that spreadsheet apps such as excel can
       open them
       This one consists of the fields:
       -Location ID
       -Account Name
       -String of locationID and speedtype united by an under score (LocationID + '_' + account_speedtype)
       -Meter read for black and white (User-entered)
       -Meter Read for color (User-entered)
       -Meter read for black and white added to meter read for color
    """

    def __init__(self, fill_fields):
        """Constructor for the CSVForm class 
        :fill fields: array of tuples that out lines the details 
        """
        super(CSVForm, self).__init__(UI_PATH)

        for i in range(0, len(fill_fields)):
            self.meter_grid.addWidget(QtWidgets.QLabel(str(fill_fields[i][0])), i, 0)
            self.meter_grid.addWidget(QtWidgets.QLabel(str(fill_fields[i][1])), i, 1)
            self.meter_grid.addWidget(QtWidgets.QLabel(str(fill_fields[i][3])), i, 2)
            self.meter_grid.addWidget(QtWidgets.QLabel(str(fill_fields[i][0])+'_'+str(fill_fields[i][2])), i, 3)
            self.meter_grid.addWidget(QtWidgets.QSpinBox(), i, 4)
            self.meter_grid.addWidget(QtWidgets.QSpinBox(), i, 5)
            self.meter_grid.itemAtPosition(i, 4).widget().setMaximum(999999)
            self.meter_grid.itemAtPosition(i, 5).widget().setMaximum(999999)
            self.meter_grid.itemAtPosition(i, 4).widget().setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
            self.meter_grid.itemAtPosition(i, 5).widget().setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)



    def get_field_data(self):
        '''Returns a tuple containing the data provided by the user'''
        items = []
        for item in range(0, self.meter_grid.rowCount()):
            meter_real_bw = self.meter_grid.itemAtPosition(item, 4).widget().value()
            meter_read_color = self.meter_grid.itemAtPosition(item, 5).widget().value()
            items.append((
                    self.meter_grid.itemAtPosition(item, 0).widget().text(),
                    self.meter_grid.itemAtPosition(item, 1).widget().text(),
                    self.meter_grid.itemAtPosition(item, 2).widget().text(),
                    (meter_real_bw+meter_read_color),
                    meter_real_bw,
                    meter_read_color
                )
            )
        return items
