This app is designed to allow the tracking and managaing payment of copiers for the University's print and copy services.

Its features inculde:

-A database (See the SQL folder for more information)
-An interface (See the dialogs folder for more information)
-The ability to write a file called a 'CSV', or Comma-Separated Value file, these can be loaded into spreadsheets.

The main program file is copier_organizer.py

This app was written using:
-Python 3.6.5
-PyQt5
-Sqlite3 (Built-In with python)

Broad overview of how the software operates:

-Running the copier_organizer.py  opens up the Main Window.
-The program connects to a database (All connections are facilitated through the "get_db_connection" function, because it opens up a single connection and ensures a database exists)
-Opening dialogs opens a new window where information is entered. This information is recorded in a tuple. All forms inherit from the "DataForm" class found dialogs/abstract_data_form.py
-The tuple is passed into some class that inherits from the DatabaseInterface class found in the SQL/db_interface.py, ensuring that changing the database backend only involves
changing the DatabaseInterface class, provided it still uses SQL.

-All errors are caught by the class in error_handling/error_handler.py using a decorator, if it throws an exception, use the error handler class to catch it and inform the user
