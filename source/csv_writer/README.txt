Contains one file, the csv writer. Uses a built-in python module to generate special files (Comma Separated Values).

the resulting file has 4 important files:
-location number: this is how the Print and Copy team track who's billed for thier copier
-Location name: The name of the location of that copier, as a string.
-Location number: united to a six-digit number via an underscore: The six-digit number is the speedtype, a number that the university finace team uses for billing.
-Sum of next two fields
-Black and white price (User-Entered)
-Color Price("Click") (User-Entered)
