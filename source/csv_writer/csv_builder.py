import csv

def write_csv(new_csv_file, items):
    with open(new_csv_file, 'w') as csv_file:
        writer = csv.writer(csv_file, delimiter=",", lineterminator='\r\n')
        writer.writerows(items)
