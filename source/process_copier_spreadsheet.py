import csv
from SQL import build_tables
import sqlite3
from SQL import copier_statements
from SQL import admin_statements
from SQL import location_statements


connection = build_tables.get_db_connection('SQL/copier_database.db')
copier_actions = copier_statements.CopierStatements(connection)
admin_actions = admin_statements.AdminStatements(connection)
location_actions = location_statements.LocationStatements(connection)


with open('MeterRead_withInfo.csv', 'r') as copiers:
    #for each line in that file
    reader = csv.reader(copiers)
    for line in reader:
        for word in range(0, len(line)):
            if not line[word]:
                line[word] = 'N/A'

        while len(line) < 19:
            line.append('NULL')
        print(len(line))
        #if the admin isn't in the database, add them
        try:
            #columns: (4, 5, 6)
            if not admin_actions.find_admin(line[4]):
                admin_actions.insert_admin((line[4], line[5], line[6]))
        except sqlite3.IntegrityError:
            print(line)
            pass
        try:
            #columns (1, 0)
            location_actions.insert_location((line[1], line[0]))
        except sqlite3.IntegrityError:
            print(line)
            pass
            
        #add the copier to the database
        try:
            #columns: 16. 2. 3. 7. 8. 10. 11. 15. 17. 18. 4. 1.
            copier_actions.insert_copier((
                    line[16], line[2], line[3], line[7], line[8], line[10], line[11], line[15], line[17], line[18], line[4], line[1]
                    ))
        except sqlite3.IntegrityError:
            print(line)
            pass
