from PyQt5 import QtWidgets, uic
import os


ui_error = '../views/error_dialog.ui'
UI_PATH = os.path.join(os.path.dirname(__file__), ui_error)

class ErrorDialog(QtWidgets.QDialog):

    """Abstract Base Class that handles the construction of UIs
    Forms that exist in this program inherit from this class.
    
    """

    def __init__(self, message):
        """Constuctor for any Dialog class

        :UI_PATH: Path to a ui file that each child class interacts with

        """
        super(ErrorDialog, self).__init__()

        uic.loadUi(UI_PATH, self)

        self.okayButton.clicked.connect(self.close)

        self.error_label.setText(message)

        self.exec_()

