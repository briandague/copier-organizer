import os, logging, sys
from .error_dialog import ErrorDialog

logger = logging.getLogger()
log_path = (os.path.join(os.path.dirname(__file__), '../logs/logging.txt'))

if not os.path.exists(log_path):
    os.mkdir(os.path.join(os.path.dirname(__file__), '../logs'))
    os.mknod(log_path)

log_handler = logging.FileHandler(log_path)
log_format = logging.Formatter('%(asctime)s:%(message)s')
log_handler.setFormatter(log_format)
logger.addHandler(log_handler)


def handle_error(func):
    """
    If the decorated function tries adding something to the database
    with a primary key that already exists, puts up a window that
    informs the user that it already exists in the database
    """

        #executing the function
    def wrapper(self=None):
        try:
            return func(self)
        except Exception as e:
            #log the error
            logger.exception(e)
            #open a dialog that explains the problem.
            ErrorDialog(str(e))



    return wrapper

