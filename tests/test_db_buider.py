import unittest, sqlite3, os
from source.SQL import build_tables

class TestDataBase(unittest.TestCase):

    def setUp(self):
        self.connection = build_tables.get_db_connection('test.db')
        self.cursor = self.connection.cursor()

    def tearDown(self):
        os.remove('test.db')

    def get_fields(self, table):
        self.cursor.execute('SELECT * FROM {}'.format(table))
        return [description[0] for description in self.cursor.description]

    def test_datbase_created(self):
        self.assertTrue(os.path.isfile('test.db'))

    def test_copier_table(self):
        description = self.get_fields('copiers')
        self.assertEqual(description, ['copier_serial', 'ipaddress', 'hostname', 'room', 'model', 'paper_source', 'finisher', 'stapler_number', 'price', 'color_price', 'location_id', 'admin'])

    def test_admin_table(self):
        admins = self.get_fields('admins')
        self.assertEqual(admins, ['id', 'name', 'email', 'phone'])

    def test_location_table(self):
        loc = self.get_fields('locations')
        self.assertEqual(loc, ['location_id', 'department'])

    def test_account(self):
        accounts=self.get_fields('accounts')
        self.assertEqual(accounts, ['speedtype', 'department', 'location_id'])

    def test_toner(self):
        toner=self.get_fields('toner')
        self.assertEqual(toner, ['toner_number', 'copier'])

    def test_waste_toner(self):
        waste=self.get_fields('waste_toner')
        self.assertEqual(waste, ['waste_toner_number', 'toner'])
        

