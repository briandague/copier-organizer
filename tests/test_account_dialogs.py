import unittest, sys

from source.dialogs import account_dialogs
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

app = QApplication(sys.argv)

class TestAccountForms(unittest.TestCase):
    
    def setUp(self):
        self.accountForm = account_dialogs.AccountForm()
        self.account_data = ('123456', 'Accounting', '2')

    def test_submit_account_data(self):
        self.accountForm.accountDepartmentText.setText('Accounting')
        self.accountForm.accountSpeedtypeSpinbox.setValue(123456)
        self.accountForm.accountLocationCombobox.addItem('2')
        self.accountForm.accountLocationCombobox.setCurrentText('2')
        submit = self.accountForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(len(self.accountForm.submitted_data), 3)
        self.assertEqual(self.accountForm.submitted_data, self.account_data)


    def test_modify_account_data(self):
        boring = ('123456', 'A Fun Department', '2')
        self.accountForm.set_field_data(boring)
        self.assertEqual(self.accountForm.accountLocationCombobox.currentText(), '2')
        self.accountForm.accountDepartmentText.setText('A Fun Department')
        submit = self.accountForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(self.accountForm.submitted_data, boring)
