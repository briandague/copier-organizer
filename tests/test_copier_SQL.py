import unittest, sqlite3

from source.SQL import copier_statements, build_tables

class TestCopierSQL(unittest.TestCase):

    def setUp(self):
        self.connection = build_tables.get_db_connection(':memory:')
        self.cursor = self.connection.cursor()
        self.copier_actions = copier_statements.CopierStatements(self.connection)
        #Toy data
        self.copier_data=('serial', 'ip', 'host', 'room', 'model', 'ps', 'finish', 'sn', 12.0, 2.0, None, None)
        self.cipier_data=('cerial', 'ip', 'hist', 'riim', 'midel', 'ps', 'finish', 'sn', 12.0, 2.0, None, None)
        self.copoer_data=('seroal', 'op', 'host', 'room', 'model', 'ps', 'fonosh', 'sn', 12.0, 2.0, None, None)

    
    def test_insert(self):
        self.copier_actions.insert_copier(self.copier_data)
        self.copier_actions.insert_copier(self.cipier_data)
        self.copier_actions.insert_copier(self.copoer_data)
        self.cursor.execute('SELECT * FROM copiers')
        copiers_inserted = self.cursor.fetchall()
        self.assertEqual(copiers_inserted, [self.copier_data, self.cipier_data, self.copoer_data])

    def test_modify(self):
        self.copier_actions.insert_copier(self.copier_data)
        update = ('serial', 'ip', 'host', 'room', 'model', 'ps', 'done', 'sn', 12.0, 2.0, None, None)
        self.copier_actions.modify_copier(update)
        modify = self.cursor.execute('SELECT * FROM COPIERS WHERE copier_serial = ?', ('serial',))
        self.assertEqual(update, modify.fetchone())

    def test_get_all(self):
        copiers = [self.copier_data, self.cipier_data, self.copoer_data]
        self.cursor.executemany('''
        INSERT INTO 
        copiers(copier_serial, ipaddress, hostname, room, model, paper_source, finisher, stapler_number, price, color_price, location_id, admin)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?)''', copiers)
        copiers_gotten = self.copier_actions.get_all()
        self.assertEqual(copiers, copiers_gotten) 

    def test_delete(self):
        copiers = [self.copier_data, self.cipier_data, self.copoer_data]
        self.cursor.executemany('''
        INSERT INTO 
        copiers(copier_serial, ipaddress, hostname, room, model, paper_source, finisher, stapler_number, price, color_price, location_id, admin)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?)''', copiers)
        self.copier_actions.remove('serial')
        self.copier_actions.remove('cerial')
        self.copier_actions.remove('seroal')
        self.assertEqual(self.copier_actions.get_all(), [])

    def test_search(self):
        copiers = [self.copier_data, self.cipier_data, self.copoer_data]
        self.cursor.executemany('''
        INSERT INTO 
        copiers(copier_serial, ipaddress, hostname, room, model, paper_source, finisher, stapler_number, price, color_price, location_id, admin)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?)''', copiers)
        self.assertEqual(self.copier_actions.search('serial'), self.copier_data) 
