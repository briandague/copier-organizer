import unittest, sys

from source.dialogs import CSV_dialog
from PyQt5 import QtWidgets
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

app = QtWidgets.QApplication(sys.argv)

class TestCSVForms(unittest.TestCase):
    
    def setUp(self):
        self.fill_data = [
                (1, "A", 23456),
                (2, "B", 78910),
                (3, "C", 111213),
                (4, "D", 141516),
                (5, "E", 171819)
                ]
        self.CSVForm = CSV_dialog.CSVForm(self.fill_data)

    def test_field_fill(self):
        self.assertEqual(self.CSVForm.meter_grid.count(), 25)

    def test_get_data(self):
        for i in range(0, 5):
            self.CSVForm.meter_grid.itemAtPosition(i, 3).widget().setValue(1234)
            self.CSVForm.meter_grid.itemAtPosition(i, 4).widget().setValue(6789)

        end_values = [
                ('1', 'A', '1_23456', 8023, 1234, 6789),
                ('2', 'B', '2_78910', 8023, 1234, 6789),
                ('3', 'C', '3_111213', 8023, 1234, 6789),
                ('4', 'D', '4_141516', 8023, 1234, 6789),
                ('5', 'E', '5_171819', 8023, 1234, 6789),
                ]
        submit= self.CSVForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)

        self.assertEqual(self.CSVForm.submitted_data, end_values)
