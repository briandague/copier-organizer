import unittest
from source.SQL import account_statements, location_statements, locations_accounts_join, build_tables

class TestAccountStatements(unittest.TestCase):
    
    def setUp(self):
        self.connection = build_tables.get_db_connection(':memory:')
        self.cursor = self.connection.cursor()
        self.account_actions =account_statements.AccountStatements(self.connection)
        self.account = (123456, 'Taco Stand', 1)
        self.acciunt = (789101, 'HR', 2)
        self.acceunt = (112131, 'Kitchen Sink', 3)
        self.accuunt = (996633, 'Alternative Sink', 3)
        self.account_actions.insert_account(self.account)
        self.account_actions.insert_account(self.acciunt)
        self.account_actions.insert_account(self.acceunt)
        self.account_actions.insert_account(self.accuunt)


        self.location_actions =location_statements.LocationStatements(self.connection)
        self.location = (1, 'Ministry of Silly Walks')
        self.licatiin = (2, 'Mr. Rogers Neigborhood')
        self.lecatien = (3, 'History')
        self.location_actions.insert_location(self.location)
        self.location_actions.insert_location(self.licatiin)
        self.location_actions.insert_location(self.lecatien)

        self.join_actions = locations_accounts_join.JoinAccountsLocations(self.connection)


    def test_join(self):
        first_join = (self.location+self.account)
        second_join = (self.licatiin+self.acciunt)
        third_join = (self.lecatien+self.acceunt)
        fourth_join = (self.lecatien+self.accuunt)
        joined = [first_join, second_join, third_join, fourth_join]
        SQL_join = self.join_actions.join_accounts_locations()  
        print(SQL_join)
        self.assertEqual(SQL_join, joined)

