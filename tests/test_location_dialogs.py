import unittest, sys

from source.dialogs import location_dialogs
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

app = QApplication(sys.argv)

class TestLocationForms(unittest.TestCase):
    
    def setUp(self):
        self.locationForm = location_dialogs.LocationForm()
        self.location_data = (10, 'Taco Bell Kitchen')

    def test_submit_location_data(self):
        self.locationForm.locationIDSpinBox.setValue(10)
        self.locationForm.locationDepartmentText.setText('Taco Bell Kitchen')
        submit = self.locationForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(len(self.locationForm.submitted_data), 2)
        self.assertEqual(self.locationForm.submitted_data, self.location_data)


    def test_modify_location_data(self):
        boring = (10, 'Kitchen')
        self.locationForm.set_field_data(boring)
        self.locationForm.locationDepartmentText.setText('Kitchen')
        submit = self.locationForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(self.locationForm.submitted_data, boring)
