import unittest
from source.SQL import location_statements, build_tables

class TestLocationStatements(unittest.TestCase):
    
    def setUp(self):
        self.connection = build_tables.get_db_connection(':memory:')
        self.cursor = self.connection.cursor()
        self.location_actions =location_statements.LocationStatements(self.connection)
        self.location = (1, 'Ministry of Silly Walks')
        self.licatiin = (2, 'Mr. Rogers Neigborhood')
        self.lecatien = (3, 'History')


    def test_insert_location(self):
        self.location_actions.insert_location(self.location)
        self.location_actions.insert_location(self.licatiin)
        self.location_actions.insert_location(self.lecatien)
        self.cursor.execute('SELECT * from locations')
        locations = self.cursor.fetchall()
        self.assertEqual(locations, [self.location, self.licatiin, self.lecatien])

    def test_update_location(self):
        self.location_actions.insert_location(self.lecatien)
        location2 = (3, 'UNSC')
        self.location_actions.modify_location(location2)
        modified = self.cursor.execute('SELECT * FROM locations WHERE location_id=?', ('3'))
        location_found = modified.fetchall()
        self.assertEqual(len(location_found), 1)
        self.assertEqual(location2, location_found[0])

    def test_remove_location(self):
        self.location_actions.insert_location(self.location)
        self.location_actions.insert_location(self.licatiin)
        self.location_actions.remove_location(self.location[0])
        locations = self.cursor.execute('SELECT * FROM locations')
        location = locations.fetchall()
        self.assertEqual(len(location), 1)
        self.assertEqual(self.licatiin, location[0])

    def test_get_location(self):
        self.location_actions.insert_location(self.location)
        self.location_actions.insert_location(self.licatiin)
        self.location_actions.insert_location(self.lecatien)
        all_locations = self.location_actions.get_all()
        self.assertEqual(all_locations, [self.location, self.licatiin, self.lecatien])
