import unittest, sys

from source.dialogs.copier_dialog import CopierDataForm
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

app = QApplication(sys.argv)

class TestCopierStatements(unittest.TestCase):

    def setUp(self):
        self.copierForm = CopierDataForm()
        self.copier_data = ('copierSerial', 'ip', 'host', 'room', 'model', 'paper', 'finish', 'staple', 12.3, 99, '', '')

    def test_submit_copier_data(self):
        self.copierForm.copierSerialText.setText('copierSerial')
        self.copierForm.ipText.setText('ip')
        self.copierForm.hostNameText.setText('host')
        self.copierForm.roomNumberText.setText('room')
        self.copierForm.modelNumberText.setText('model')
        self.copierForm.paperSourceText.setText('paper')
        self.copierForm.finisherText.setText('finish')
        self.copierForm.staplerNumberText.setText('staple')
        self.copierForm.priceBWSpinBox.setValue(12.3)
        self.copierForm.priceColorSpinBox.setValue(99)
        submit_button = self.copierForm.submitButton
        QTest.mouseClick(submit_button, Qt.LeftButton)
        self.assertEqual(self.copierForm.submitted_data, self.copier_data)
        

    def test_modify_copier(self):
        submit = self.copierForm.submitButton
        change = ('copierSerial', 'address', 'host', 'room', 'model', 'paper', 'finish', 'staple', 12.3, 99, '', '')
        self.copierForm.set_field_data(change)
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(self.copierForm.ipText.toPlainText(), 'address')
        self.assertEqual(self.copierForm.finisherText.toPlainText(), 'finish')
        self.assertEqual(self.copierForm.submitted_data, change)
