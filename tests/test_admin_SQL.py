import unittest
from source.SQL import admin_statements, build_tables

class TestAdminStatements(unittest.TestCase):
    
    def setUp(self):
        self.connection = build_tables.get_db_connection(':memory:')
        self.cursor = self.connection.cursor()
        self.admin_actions =admin_statements.AdminStatements(self.connection)
        self.admin = ('Bob', 'bob@bob.bob', '808')
        self.admon = ('CJ', 'CJDJ@jam.mon', '999')
        self.admen = ('Alice', 'A-list@list.a', '416')


    def test_insert_admin(self):
        self.admin_actions.insert_admin(self.admin)
        self.admin_actions.insert_admin(self.admon)
        self.admin_actions.insert_admin(self.admen)
        self.cursor.execute('SELECT * from admins')
        admins = self.cursor.fetchall()
        admins = [admin[1:] for admin in admins]
        self.assertEqual(admins, [self.admin, self.admon, self.admen])

    def test_update_admin(self):
        self.admin_actions.insert_admin(self.admin)
        admin2 = ('Bob', 'tim@tim.tim', '413')
        self.admin_actions.modify_admin(admin2)
        modified = self.cursor.execute('SELECT * FROM admins WHERE name=?', ('Bob',))
        admin_found = modified.fetchall()
        self.assertEqual(len(admin_found), 1)
        self.assertEqual(admin2, admin_found[0][1:])

    def test_remove_admin(self):
        self.admin_actions.insert_admin(self.admin)
        self.admin_actions.insert_admin(self.admon)
        self.admin_actions.remove_admin(self.admin[0])
        admins = self.cursor.execute('SELECT * FROM admins')
        admin = admins.fetchall()
        self.assertEqual(len(admin), 1)
        self.assertEqual(self.admon, admin[0][1:])

    def test_get_admin(self):
        self.admin_actions.insert_admin(self.admin)
        self.admin_actions.insert_admin(self.admon)
        self.admin_actions.insert_admin(self.admen)
        all_admins = self.admin_actions.get_all()
        all_admins = [admin[1:] for admin in all_admins]
        self.assertEqual(all_admins, [self.admin, self.admon, self.admen])
