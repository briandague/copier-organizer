import unittest
from source.SQL import account_statements, build_tables

class TestAccountStatements(unittest.TestCase):
    
    def setUp(self):
        self.connection = build_tables.get_db_connection(':memory:')
        self.cursor = self.connection.cursor()
        self.account_actions =account_statements.AccountStatements(self.connection)
        self.account = (123456, 'Taco Stand', 808)
        self.acciunt = (789101, 'HR', 999)
        self.acceunt = (112131, 'Kitchen Sink', 416)


    def test_insert_account(self):
        self.account_actions.insert_account(self.account)
        self.account_actions.insert_account(self.acciunt)
        self.account_actions.insert_account(self.acceunt)
        self.cursor.execute('SELECT * from accounts')
        accounts = self.cursor.fetchall()
        self.assertEqual(accounts, [self.account, self.acciunt, self.acceunt])

    def test_update_account(self):
        self.account_actions.insert_account(self.account)
        account2 = (123456, 'Red Room', 413)
        self.account_actions.modify_account(account2)
        modified = self.cursor.execute('SELECT * FROM accounts WHERE speedtype=?', ('123456',))
        account_found = modified.fetchall()
        self.assertEqual(len(account_found), 1)
        self.assertEqual(account2, account_found[0])

    def test_remove_account(self):
        self.account_actions.insert_account(self.account)
        self.account_actions.insert_account(self.acciunt)
        self.account_actions.remove_account(self.account[0])
        accounts = self.cursor.execute('SELECT * FROM accounts')
        account = accounts.fetchall()
        self.assertEqual(len(account), 1)
        self.assertEqual(self.acciunt, account[0])

    def test_get_account(self):
        self.account_actions.insert_account(self.account)
        self.account_actions.insert_account(self.acciunt)
        self.account_actions.insert_account(self.acceunt)
        all_accounts = self.account_actions.get_all()
        self.assertEqual(all_accounts, [self.account, self.acciunt, self.acceunt])
