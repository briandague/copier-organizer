import unittest, sys

from source.dialogs import admin_dialogs
from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

app = QApplication(sys.argv)

class TestAdminForms(unittest.TestCase):
    
    def setUp(self):
        self.adminForm = admin_dialogs.AdminForm()
        self.admin_data = ('Steve', 'TheStevester@steven.ven', '573')

    def test_submit_admin_data(self):
        self.adminForm.adminEmailText.setText('TheStevester@steven.ven')
        self.adminForm.adminNameText.setText('Steve')
        self.adminForm.adminPhoneText.setText('573')
        submit = self.adminForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(len(self.adminForm.submitted_data), 3)
        self.assertEqual(self.adminForm.submitted_data, self.admin_data)


    def test_modify_admin_data(self):
        boring = ('Steve', 'bosssaysicanthavefunemails@boring.nofun', '573')
        self.adminForm.set_field_data(boring)
        self.assertEqual(self.adminForm.adminPhoneText.toPlainText(), '573')
        self.adminForm.adminEmailText.setText('bosssaysicanthavefunemails@boring.nofun')
        submit = self.adminForm.submitButton
        QTest.mouseClick(submit, Qt.LeftButton)
        self.assertEqual(self.adminForm.submitted_data, boring)
