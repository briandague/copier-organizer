Contains classes that instantiate 

All of the classes here rely on the DataForm class, which intitializes thier UI
and sets the class variable to be returned as a usuable value.

Dataform works like this:

-the get_data() method is called by an outside class 

-the get_field_data method must be defined by the child class. It's the method that takes the data from the form's fields and passes it into the submitted_data class variable. This should almost always return a tuple, because it's usually passed into a SQL query. 
(However, it could theoratically pass back whatever type the programmer wished)

-The user clicking the submitButton in the GUI causes the set_submit_data method to trigger, 
which calls get_field_data, and closes the dialog window.

Any class the inherits from DataForm should:
    -Have a get_field_data method.
    -Have a ui that includes a button called "submitButton"


Note: QT has a special widget call "SpinBox", which only allows numerical input data. Currently, all
strings are submitted though a widget called, "QTextEdit", and all integer data is entered via these
spinboxes. This means users cannot enter letters into a Numerical field. 

