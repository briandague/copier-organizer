from .abstract_data_form import DataForm
import os


UI_location = '../views/location_dialog.ui'

UI_PATH = os.path.join(os.path.dirname(__file__), UI_location)
class LocationForm(DataForm):

    """Form foradding locations, 3 fields"""

    def __init__(self):
        """Constructor forthe location class """
        super(LocationForm, self).__init__(UI_PATH)

    def get_field_data(self):
        '''Returns a tuple containing the data provided by the user'''
        return (
                self.locationIDSpinBox.value(),
                self.locationDepartmentText.toPlainText(),
                )

    def set_field_data(self, data):
        """Changes the form's textfields

        :data: tuple of whatever values user might change

        """
        self.locationIDSpinBox.setValue(int(data[0]))
        self.locationDepartmentText.setText(data[1])


    def add_location(self):
        """Opens the dialog and allows fields to be recorded
        :returns: a tuple of the fields filled out

        """
        return self.get_data()


    def modify_location(self, data):
        """ Opens a dialog forthe location with stuff pre-filled so 
        user doesn't have to enter it again 
        :data: tuple, sets inorder from topto bottom
        :returns: a tuple of what's inthe fields when the user hits submit
        """
        self.set_field_data(data)
        return self.get_data()
