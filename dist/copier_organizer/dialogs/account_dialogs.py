from .abstract_data_form import DataForm
import os


UI_account = '../views/account_dialog.ui'

UI_PATH = os.path.join(os.path.dirname(__file__), UI_account)
class AccountForm(DataForm):

    """Form foradding accounts, 3 fields"""

    def __init__(self, locations=None):
        """Constructor forthe account class """
        super(AccountForm, self).__init__(UI_PATH)
        if locations:
            self.accountLocationCombobox.addItems(locations)

    def get_field_data(self):
        '''Returns a tuple containing the data provided by the user'''
        return (
                str(self.accountSpeedtypeSpinbox.value()),
                self.accountDepartmentText.toPlainText(),
                self.accountLocationCombobox.currentText()
                )

    def set_field_data(self, data):
        """Changes the form's textfields

        :data: tuple of whatever values user might change

        """
        self.accountSpeedtypeSpinbox.setValue(int(data[0]))
        self.accountDepartmentText.setText(data[1])
        self.accountLocationCombobox.addItem(data[2])
        self.accountLocationCombobox.setCurrentText(data[2])


    def add_account(self):
        """Opens the dialog and allows fields to be recorded
        :returns: a tuple of the fields filled out

        """
        return self.get_data()


    def modify_account(self, data):
        """ Opens a dialog forthe account with stuff pre-filled so 
        user doesn't have to enter it again 
        :data: tuple, sets inorder from topto bottom
        :returns: a tuple of what's inthe fields when the user hits submit
        """
        self.set_field_data(data)
        return self.get_data()
