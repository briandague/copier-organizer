from PyQt5 import QtWidgets, uic
import abc

class DataForm(QtWidgets.QDialog):

    """Abstract Base Class that handles the construction of UIs
    Forms that exist in this program inherit from this class.
    
    """

    def __init__(self, UI_PATH):
        """Constuctor for any Dialog class

        :UI_PATH: Path to a ui file that each child class interacts with

        """
        super(DataForm, self).__init__()

        uic.loadUi(UI_PATH, self)

        self.submitButton.clicked.connect(self.set_submit_data)

        self.submitted_data = None

    def set_submit_data(self):
        """In Implementations, this is for: 
        -setting the submitted_data class variable as a tuple from the form's fields
        -calling self.accept(), method from the QtDialog that closes the
        form and passes control back to the main app

        """
        self.submitted_data = self.get_field_data()
        self.accept()

    @abc.abstractmethod
    def get_field_data(self):
        """Use this method in child classes to build a tuple from
        data in each form's fields
        :returns: A tuple containing data in the fields

        """
        raise NotImplementedError("All DataForm implementations need a get_field_data method")
    
    def get_data(self):
        """Takes data entered by theuser and outputs itas a tuple
        :returns: tuple containing data from the DataForm's get_form_data
        method.

        """
        if self.exec_():
            return self.submitted_data
