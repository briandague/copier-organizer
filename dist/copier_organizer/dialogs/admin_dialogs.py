from .abstract_data_form import DataForm
import os


UI_admin = '../views/admin_dialog.ui'

UI_PATH = os.path.join(os.path.dirname(__file__), UI_admin)
class AdminForm(DataForm):

    """Form foradding admins, 3 fields"""

    def __init__(self):
        """Constructor forthe admin class """
        super(AdminForm, self).__init__(UI_PATH)

    def get_field_data(self):
        '''Returns a tuple containing the data provided by the user'''
        return (
                self.adminNameText.toPlainText(),
                self.adminEmailText.toPlainText(),
                self.adminPhoneText.toPlainText()
                )

    def set_field_data(self, data):
        """Changes the form's textfields

        :data: tuple of whatever values user might change

        """
        self.adminNameText.setText(data[0])
        self.adminEmailText.setText(data[1])
        self.adminPhoneText.setText(data[2])


    def add_admin(self):
        """Opens the dialog and allows fields to be recorded
        :returns: a tuple of the fields filled out

        """
        return self.get_data()


    def modify_admin(self, data):
        """ Opens a dialog forthe admin with stuff pre-filled so 
        user doesn't have to enter it again 
        :data: tuple, sets inorder from topto bottom
        :returns: a tuple of what's inthe fields when the user hits submit
        """
        self.set_field_data(data)
        return self.get_data()
