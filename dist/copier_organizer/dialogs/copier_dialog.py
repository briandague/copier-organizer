import os, sys
from .abstract_data_form import DataForm

ui_add_copier = '../views/add_copiers.ui'

UI_PATH = os.path.join(os.path.dirname(__file__), ui_add_copier)

class CopierDataForm(DataForm):

    """Adapter that takes the data from the copier form and transfers it into the application"""

    def __init__(self, admins=None, locations=None):
        """Constructor class for CopierDataForm, just instantiates the super class with a UI
        :admins: List of admins in the database, fills in copierAdminComboBox
        :locations: List of locations in the database, fills in the copierLocationCombobox
        """
        super(CopierDataForm, self).__init__(UI_PATH)
        if admins:
            self.copierAdminCombobox.addItems(admins)
        if locations:
            self.copierLocationCombobox.addItems(locations)
        
    def get_field_data(self):
        """Gives user-supplied data for copiers
        :returns: Tuple of each value entered in the form

        """
        return (
                self.copierSerialText.toPlainText(),
                self.ipText.toPlainText(),
                self.hostNameText.toPlainText(),
                self.roomNumberText.toPlainText(),
                self.modelNumberText.toPlainText(),
                self.paperSourceText.toPlainText(),
                self.finisherText.toPlainText(),
                self.staplerNumberText.toPlainText(),
                self.priceBWSpinBox.value(),
                self.priceColorSpinBox.value(),
                self.copierAdminCombobox.currentText(),
                self.copierLocationCombobox.currentText()
                )

    def set_field_data(self, modded_data):
        """puts all the data in a fileto certain values

        :data: A nine-long tuple that conatains data for a copier, in the order
        it's run in the copierTable (see build tables)

        """
        self.copierSerialText.setText(modded_data[0]),
        self.ipText.setText(modded_data[1]),
        self.hostNameText.setText(modded_data[2]),
        self.roomNumberText.setText(modded_data[3]),
        self.modelNumberText.setText(modded_data[4]),
        self.paperSourceText.setText(modded_data[5]),
        self.finisherText.setText(modded_data[6]),
        self.staplerNumberText.setText(modded_data[7]),
        self.priceBWSpinBox.setValue(float(modded_data[8])),
        self.priceColorSpinBox.setValue(float(modded_data[9])),
    
    def add_copier(self):
        '''opens a blank form for the user to enter data'''
        self.copierSerialText.setReadOnly(False)
        return self.get_data()

    def modify_copier(self, data):
        '''Opens a pre-filled form with the modified copier data in place and the 
        serial number(primary key) of the copier unchangeable.
        :data: The whole tuple of the copier tobe modified
        '''
        self.copierSerialText.setReadOnly(True)
        self.set_field_data(data)
        return self.get_data()
