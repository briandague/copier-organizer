import sqlite3

class DatabaseInterface(object):
    '''
    Handles getting datainand out fromthe databse. All classes that interact with the 
    databsemust inherit from this class
    
    '''

    def __init__(self, connection):
        '''
        Instantation for the DatabaseInterface class

        :connection: connected Database that we communicate with
        '''

        self.connection = connection
        self.cursor = self.connection.cursor()
    
    def protect_database(func):
        '''Error-detecting decorator function, catches operational errors, rolls backthe databse, and re-raises them.'''
        def wrapper(self, statement, data=None):
            try:
                return func(self, statement, data)
            except sqlite3.OperationalError:
                self.connection.rollback()
                raise
            except sqlite3.IntegrityError as e:
                self.connection.rollback()
                err = str(e)
                problem_table = err.split()[-1]
                table_column = problem_table.split('.')
                #can only have one (table_name) with the (column_name) of (unique_key)
                print("Can only have one {} with the {} of {}".format(table_column[0], table_column[1], data[0]))
                raise
        return wrapper

    @protect_database
    def run_input_statement(self, statement, data):
        '''Runs a parameterized query
        :statement: SQL query use ? for parameterized queries
        :data: a tuple used for parameterized substution only
        '''
        self.cursor.execute(statement, data)
        self.connection.commit()
        

    @protect_database
    def run_output_statement(self, statement, data=None):
        """Runs Queries such as SELECT, that return data

        :data: a tuple used for parameterized substution only, don't include in unparameterized queries.
        :statement: SQL query. for parameterized queries
        :returns: results from the SQL queries as a list of tuples, e

        """
        if data is None:
            entries = self.cursor.execute(statement)
        else:
            entries = self.cursor.execute(statement, data)

        results = entries.fetchall()
        return results
