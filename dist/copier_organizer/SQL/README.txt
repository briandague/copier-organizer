build_tables.py -> Contains the SQL code for building the databse and all of its tables. the get_db_connection should be
the ONLY way you ever touch the database in this program, if you want to change the database later, it'll only take changing this to facilitate a connection

db_interface -> Abstract class, all classes that interact with the database should inherit from this class, it facilitates both input and output and tries to
roll back the database if things go awry. Use the connection returned from build_tables.py's get_db_connection in its constructor to minimze unneeded opening/closing of the database
(Though it only catches IntegrityErrors and OperationalErrors) You will likely need to change its class methods should
you change the database. 

All other files in here have class that inherit from database interface, and merely need a few class variables that hold SQL statements for interacting with
the database, and helper methods that basically call the input/output functions in thier parent DatabaseInterface. You should not need to modify these
if you need to change database software, and these extra methods help clear up what the code is doing. It's proabably a bad idea to call random SQL statements 
that others reading the code might have trouble interpreting, so add the statement as a class variable with a descriptive name, and a class method that describes what it does.
