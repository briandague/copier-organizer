from .db_interface import DatabaseInterface

class AccountStatements(DatabaseInterface):

    """API for interacting with the account table"""

    def __init__(self, connection):
        """Constructior for the account interfece, input an established connection.

        :connection: Pre-established sqlite3 connection

        """
        DatabaseInterface.__init__(self, connection)

        self.insert_statement = 'INSERT INTO accounts(speedtype, department, location_id)  VALUES (?, ?, ?)'
        self.update_statement = 'UPDATE accounts SET department=?,location_id=? WHERE speedtype=?'
        self.remove_statement = 'DELETE FROM accounts WHERE speedtype=?'
        self.search_statement = 'SELECT * FROM accounts WHERE speedtype=?'
        self.select_all = 'SELECT * FROM accounts'

    def insert_account(self, data):
        """method for adding accountto the database

        :data: Account's speedtype, department, and location number, in a tuple, as strings

        """
        self.run_input_statement(self.insert_statement, data)

    def modify_account(self, data):
        """For changing an account's location or department

        :data: Account's speedtype, department, and location number, in a tuple, as strings

        """
        structured_data = (data[1], data[2], data[0])
        self.run_input_statement(self.update_statement, structured_data)

    def remove_account(self, account_speedtype):
        """Deletes an account from the database

        :account_speedtype: speedtype, as string, of the account to be deleted, not the primary key 

        """
        self.run_input_statement(self.remove_statement, (account_speedtype,))

    def get_all(self):
        """Returns all the accounts in the database
        :returns: TODO

        """
        return self.run_output_statement(self.select_all)
