from .db_interface import DatabaseInterface

class CopierStatements(DatabaseInterface):

    """Class that interacts with the copier table of the database """

    def __init__(self, connection):
        """
        Instantiation for the copier class
        """
        DatabaseInterface.__init__(self, connection)
        self.insert_statement = '''
        INSERT INTO 
        copiers (copier_serial, ipaddress, hostname, room, model, paper_source, finisher, stapler_number, price, color_price, admin, location_id)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        '''
        self.modify_statement = '''
        UPDATE copiers
        SET ipaddress=?,hostname=?,room=?,model=?,paper_source=?,finisher=?,stapler_number=?,price=?,color_price=?,admin=?,location_id=?
        WHERE copier_serial=?
        '''
        self.select_all = 'SELECT * FROM copiers'

        self.remove_statement = 'DELETE FROM copiers WHERE copier_serial=?'

        self.search_statement = 'SELECT * FROM copiers WHERE copier_serial=?'


    def insert_copier(self, data):
        """Adds a new copier sans any admin or location info.

        :data: A tuple containing twelve fields:
            1. Serial Number
            2. IP Address
            3. HostName
            4. Room Number
            5. Model Number
            6. Paper Source (Paper Tray)
            7. finisher
            8. Stapler number
            9. Price for black and white jobs
            10. Price for color jobs
            11. The copier's location_ID
            12. The copier's admin

        """
        self.run_input_statement(self.insert_statement, data)
    
    def modify_copier(self, data):
        """Updates a copier's fields.

        :data: A tuple containing twelve fields:
            1. Serial Number - Used to look up the copier, can't bechanged because it's the Primary Key
            2. IP Address
            3. HostName
            4. Room Number
            5. Model Number
            6. Paper Source (Paper Tray)
            7. finisher
            8. Stapler number
            9. Price for black and white jobs
            10. Price for color jobs.
            11. The copier's location_id
            12. the copier's admin

        """
        data_that_may_change = data[1:]
        primary_key = (data[0],)
        data_structured_for_query = data_that_may_change+primary_key

        self.run_input_statement(self.modify_statement, data_structured_for_query)
    
    def get_all(self):
        """ Returns all the copiers in the database, a simple select query.
        :returns: every copierin the databse asa form of tuples.

        """
        return self.run_output_statement(self.select_all)
    
    def remove(self, primary_key):
        """Deletes a copier in the database.

        :primary_key: copier_serial for the copier to be deleted

        """
        self.run_input_statement(self.remove_statement, (primary_key,))

    def search(self, criteria):
        """Returns a single tuple based onthe primary key

        :criteria: Sting with thecopier S/N you'relooking for 
        :returns: all the copiers in the table thatmeet the criteria

        """
        return self.run_output_statement(self.search_statement, (criteria,))[0]
