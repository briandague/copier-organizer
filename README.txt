This is a prgram for helping the UPS print and copy manager track billing info,
here's a few important files in this directory.

/source:
contains all the code.
Its README contains more technical info like the architecture and the current stack.


/copierAppEnv:
Contains the environment and all the packages needed to run the program. uses a virtualenv
(Python feature, allows developers to build special envorinments to freeze build environments and
build packages)
Set up virtualenv using the shell command:

user@computer$:source copierAppEnv/bin/activate


/build:
this program uses pyinstaller to build and distribute itself.

After running the above command,run:
(copierAppEnv)user@computer$:pyinstaller source/copier_organizer.py



/tests:
all the unit tests for the code.

with the env active, run:

(copierAppEnv)user@computer$: python -m unittest discover -v

to run all tests. Write new unit tests and run all tests when you write code! They'll tell you 
when a feature breaks the current build and what module changed.

requirments.txt:
all the packages installed by pip3.


